<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

define('DB_NAME', 'admin_devnsw');

/** MySQL database username */
define('DB_USER', 'admin_devnsw');

/** MySQL database password */
define('DB_PASSWORD', 'J6KxRgA3O0');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/*define('FTP_USER', 'admin'); // Your FTP username
define('FTP_PASS', '***'); // Your FTP password
define('FTP_HOST', '203.143.89.250'); // Your FTP URL:Your FTP port

define( 'FS_CHMOD_FILE', 0644 );
define( 'FS_CHMOD_DIR', 0755 );*/

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         'v=hPf.hC}WvP%WIJ`(cn:z^vk$as63@S:CaOT?i8@b*m~kjM[s0lTyqDQ0%A(oO)');
define('SECURE_AUTH_KEY',  'Sxs`@Ncx4GK_Pu Mhj^x6zg,vJwtB[L4aQhwWPaZhUm59qS%xiF1*E8wmJG^.BJl');
define('LOGGED_IN_KEY',    'j}{GK7fKB4VK/Aa ,1#RUAfew`0/:i.p?IHx4}de!L{@=TQtI2p1hyiY8pVapl^9');
define('NONCE_KEY',        '6T@+k/syCiFYf.wfuHVgUO40>(%kZAHg{6<y2Aoi*v<U>+Y:P_k~BveG+*#&y(vK');
define('AUTH_SALT',        'M0tK(x*9k@f!VAPf1{`k/ryf:b(7@1VC*T3?U}ISk$7t$@;N)~S!EgDkJqk<]vph');
define('SECURE_AUTH_SALT', 'SLV%_zl,u^R-nSuxrM,S|PO4B`#ivd@J2-&F};BQYLla^55eHh-ERTse{(j4$zfU');
define('LOGGED_IN_SALT',   '$iG$tx)9nX}E1$%6=+H<0FaUDFiUF%%EH_OhdLgyqP! W7SKF.^*0b(`Z#NoTX{`');
define('NONCE_SALT',       ',*;c0K%+a-@,ZM+?~M(dcx]&1 :MTu9QG2 4IVyM&}=A{b>g .+ZcExu-.UkJ!G<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**open_basedir restriction fix.
 * go to /wp-content/plugins/backwpup/inc/class-job.php, line 1711
 * find      if ( $file->isDir() || $file->isDot() ) {
 * change to if ( $file->isDot() || $file->isDir() ) {
 * hit save and it should fix it */

/** Wordpress version auto-update */
define( 'WP_AUTO_UPDATE_CORE', false );