var WEEKS_PER_YEAR = 52;
var FORTNIGHTS_PER_YEAR = 26;
var MONTHS_PER_YEAR = 12;

var PRINCIPAL_AND_INTEREST = "principal and interest";
var INTEREST_ONLY = "interest only";

var LoanCalc = {
  /**
   * @param {*} str
   * @returns {String}
   */
  addCommas: function(str) {
    str += '';
    var x = str.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
  },

  /**
   * @param {string} str
   * @returns {number}
   */
  removeCommas: function(str) {
    str = str ? str.replace(/\,/g, '') : '';
    return parseFloat(str);
  },

  /**
   * @param {Array} arr
   * @returns {string}
   */
  resultToString: function(arr) {
    var str = '';
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].key === 'SEPARATOR') {
        str += "\r\n";
      } else {
        str += arr[i].key;
        str += ": \t\t";
        str += arr[i].value;
        str += "\r\n";
      }
    }
    return str;
  },

  /**
   * @param {number} loanAmount
   * @param {number} interest
   * @param {number} n
   * @param {String=} repaymentType
   * @returns {{repayment: number, total_interest: number}}
   */
  calculate: function(loanAmount, interest, n, repaymentType) {
    var repayment;
    var totalInterest;

    if (repaymentType == INTEREST_ONLY) {
      repayment = loanAmount * interest;
      totalInterest = repayment * n;
    } else {
      // see wikipedia amortization
      // http://en.wikipedia.org/wiki/Amortization_calculator
      // or http://www.ext.colostate.edu/pubs/farmmgt/03757.html
      // get number of repayments and interest for each repayment
      // http://invested.com.au/71/rental-yield-2659/
      // calculate repayment for this time period
      // http://www.hughchou.org/calc/formula.html
      repayment = (interest * loanAmount) / (1 - Math.pow((1 + interest), -n));
      totalInterest = repayment * n - loanAmount;
    }

    return {repayment: repayment, total_interest: totalInterest};
  },

  /**
   * @param {number} totalRepayment
   * @param {number} interest
   * @param {number} repaymentNumber
   * @param {number} period
   * @returns {{principal_repayment: number, interest_repayment: number}}
   */
  calculatePeriodRepayment: function(totalRepayment, interest, repaymentNumber, period) {
    // see http://www.ext.colostate.edu/pubs/farmmgt/03757.html
    var principalRepayment = totalRepayment * (Math.pow((1 + interest), -(1 + repaymentNumber - period)));
    var interestRepayment = totalRepayment - principalRepayment;

    return {
      principal_repayment: principalRepayment,
      interest_repayment: interestRepayment
    };
  },

  /**
   * @ex PeriodRepayment
   *
   * @param {number} rate
   * @param {number} pmt
   * @param {number} pv
   * @param {number} per
   * @returns {{principal_payment: number, interest_payment: number}}
   */
  periodRepayment: function(rate, pmt, pv, per) {
    var interestPayment = 0;
    var principalPayment = 0;
    var a = pv;
    for (var i = 0; i <= per; i++) {
      interestPayment = a * rate;
      principalPayment = pmt - interestPayment;
      a -= principalPayment;
    }
    return {principal_payment: principalPayment, interest_payment: interestPayment};
  },

  /**
   * @ex TotalInterest
   *
   * @param {number} rate
   * @param {number} nper
   * @param {number} pmt
   * @param {number} pv
   * @returns {number}
   */
  totalInterest: function(rate, nper, pmt, pv) {
    var totalInterest = 0;
    var a = pv;
    var interestPayment;
    var principalPayment;

    for (var i = 0; i < nper; i++) {
      interestPayment = a * rate;
      principalPayment = pmt - interestPayment;
      a -= principalPayment;
      totalInterest += interestPayment;
    }

    return totalInterest;
  },

  /**
   * @ex Total
   *
   * @param {number} rate
   * @param {number} nper
   * @param {number} pmt
   * @param {number} pv
   * @returns {{total_principal: number, total_interest: number}}
   */
  total: function(rate, nper, pmt, pv) {
    var totalPrincipal = 0;
    var totalInterest = 0;
    var a = pv;
    var interestPayment;
    var principalPayment;

    for (var i = 0; i < nper; i++) {
      interestPayment = a * rate;
      principalPayment = pmt - interestPayment;
      a -= principalPayment;

      totalPrincipal += principalPayment;
      totalInterest += interestPayment;
    }

    return {total_principal: totalPrincipal, total_interest: totalInterest};
  },

  /**
   * @param {number} rate
   * @param {number} pmt
   * @param {number} pv
   * @param {number} fv
   * @returns {number}
   */
  NPER: function(rate, pmt, pv, fv) {
    rate = parseFloat(rate);
    pmt = parseFloat(pmt);
    pv = parseFloat(pv);

    if (rate == 0) {
      return -(fv + pv) / pmt;
    } else {
      return Math.ceil(Math.log((-fv * rate + pmt) / (pmt + rate * pv)) / Math.log(1 + rate));
    }
  },

  /**
   * @param {number} rate
   * @param {number} nper
   * @param {number} pv
   * @param {number} fv
   * @returns {number}
   * @constructor
   */
  PMT: function(rate, nper, pv, fv) {
    rate = parseFloat(rate);
    nper = parseFloat(nper);
    pv = parseFloat(pv);

    if (rate == 0) {
      return -(fv + pv) / nper;
    } else {
      var x = Math.pow(1 + rate, nper);
      return (rate * (fv + x * pv)) / (-1 + x);
    }
  },

  /**
   * @param {number} rate
   * @param {number} nper
   * @param {number} pmt
   * @param {number} pv
   * @returns {number}
   */
  FV: function(rate, nper, pmt, pv) {
    rate = parseFloat(rate);
    pmt = parseFloat(pmt);
    nper = parseFloat(nper);

    if (rate == 0) {
      return -(pv + (pmt * nper));
    } else {
      var x = Math.pow(1 + rate, nper);
      return -(-pmt + x * pmt + rate * x * pv ) /rate;
    }
  },

  /**
   * @param {number} rate
   * @param {number} nper
   * @param {number} pmt
   * @param {number} fv
   * @returns {number}
   */
  PV: function(rate, nper, pmt, fv) {
    rate = parseFloat(rate);
    pmt = parseFloat(pmt);
    nper = parseFloat(nper);

    if (pmt == 0 || nper == 0) {
      return 0;
    } else {
      if ( rate == 0 ) {
        return -(fv + (pmt * nper));
      } else {
        var x = Math.pow(1 + rate, -nper);
        var y = Math.pow(1 + rate, nper);
        return -( x * (fv * rate - pmt + y * pmt )) / rate;
      }
    }
  },

  /**
   * @param {{from: string, to: string, subject: string, message: string}} data
   * @returns {boolean}
   */
  validateMailForm: function(data) {
    var errorMessage = "";
    var $errorMessage = $(".calc #errmsg").html(errorMessage);

    if (data.to == null || data.to == "")
      errorMessage += "friends email address is required<br>";

    if (data.subject == null || data.subject == "") {
      errorMessage += "subject is required<br>";
    }

    if (data.message == null || data.message == "") {
      errorMessage += "message is required<br>";
    }

    var atPosition = data.to.indexOf("@");
    var dotPosition = data.to.lastIndexOf(".");

    if (atPosition < 1 || dotPosition < atPosition + 2 || dotPosition + 2 >= to.length) {
      errorMessage += "invalid friend email address<br>";
    }

    if (errorMessage.length > 0) {
      $errorMessage.html(errorMessage);
      return false;
    }

    return true;
  },

  /**
   *
   */
  sendMail: function() {
    var $form = $("#email-form form");
    var data = {
      from: $form.find("#from").val(),
      to: $form.find("#to").val(),
      subject: $form.find("#subject").val(),
      message: $form.find("#message").val()
    };

    if (LoanCalc.validateMailForm(data)) {
      $form.find("#email-response").html("<img src='images/ajax-loader2.gif" + "' />");
      $.ajax({ url: $form.attr("action"),
        data: data,
        type: 'GET',
        async: false,
        contentType: "application/json",
        dataType: 'jsonp',
        /**
         * @param {{msg:string}} output
         */
        success: function (output) {
          $form.find("#email-response").fadeIn("slow").html(output.msg);
          setTimeout(function () {
            $form.find("#email-response").fadeOut("slow")
          }, 4000);
        },
        error: function (e) {
          //console.log("AJAX Error: " + e.message);
          $form.find("#errmsg").html(e.message);
        }
      });
    }
  }
};