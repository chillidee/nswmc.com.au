(function ($) {
  var printMode = false;
  var BASE_INTEREST_RATE_BUFFER = 1.50;
  var BASE_MAX_INCOME_AVAILABLE = 85.0;

  var $calcElement = $('#calc-borrowing-power');
  if (!$calcElement.length) {
    return;
  }

  var result = [];

  function get_inputs() {
    var joint_income = $('#joint_income:checked').val();
    var dependents = parseInt($('#dependents').val());

    var net_salary_type1 = $("#net_salary_type1").val();
    var net_salary1 = LoanCalc.removeCommas($("#net_salary1").val());

    var net_salary_type2 = $("#net_salary_type2").val();
    var net_salary2 = LoanCalc.removeCommas($("#net_salary2").val());

    var other_net_income_type = $("#other_net_income_type").val();
    var other_net_income = LoanCalc.removeCommas($("#other_net_income").val());

    var max_income_available = BASE_MAX_INCOME_AVAILABLE;

    var annual_expenses = LoanCalc.removeCommas($("#annual_expenses").val());
    var annual_expenses_default = $('#annual_expenses_default:checked').val();

    var car_loan = parseFloat($("#car_loan").val());
    var credit_card = parseFloat($("#credit_card").val());

    var other_payments_type = $("#other_payments_type").val();
    var other_payments = parseFloat($("#other_payments").val());


    var interest_rate = parseFloat($("#interest_rate").val());
    var loan_term = parseFloat($("#loan_term").val());
    var interest_rate_buffer = BASE_INTEREST_RATE_BUFFER;


    if (isNaN(net_salary1))
      net_salary1 = 0;

    if (isNaN(net_salary2))
      net_salary2 = 0;

    if (isNaN(other_net_income))
      other_net_income = 0;

    if (isNaN(max_income_available))
      max_income_available = 0;

    if (isNaN(annual_expenses))
      annual_expenses = 0;

    if (isNaN(car_loan))
      car_loan = 0;

    if (isNaN(credit_card))
      credit_card = 0;

    if (isNaN(other_payments))
      other_payments = 0;

    return {joint_income: joint_income,
      dependents: dependents,
      net_salary_type1: net_salary_type1,
      net_salary1: net_salary1,
      net_salary_type2: net_salary_type2,
      net_salary2: net_salary2,
      other_net_income_type: other_net_income_type,
      other_net_income: other_net_income,
      max_income_available: max_income_available,
      annual_expenses: annual_expenses,
      annual_expenses_default: annual_expenses_default,
      car_loan: car_loan,
      credit_card: credit_card,
      other_payments_type: other_payments_type,
      other_payments: other_payments,
      interest_rate: interest_rate,
      loan_term: loan_term,
      interest_rate_buffer: interest_rate_buffer
    };
  }

  $("#interest_rate_slider").slider({ min: 0.25, max: 25, step: 0.25, range: "min", animate: "true", value: 5.99,
    slide: function (event, ui) {
      $("#interest_rate").val(ui.value);
    },
    stop: function (event, ui) {
      $("#interest_rate").val(ui.value);
      redraw();
    }
  });

  $("#loan_term_slider").slider({ min: 0.5, max: 30, step: 0.5, range: "min", animate: "true", value: 30.0,
    slide: function (event, ui) {
      $("#loan_term").val(ui.value);
    },
    stop: function (event, ui) {
      $("#loan_term").val(ui.value);
      redraw();
    }
  });

  $("#interest_rate").change(function () {
    redraw();
    $("#interest_rate_slider").slider({
      value: LoanCalc.removeCommas($("#interest_rate").val())
    });
  });

  $("#loan_term").change(function () {
    redraw();
    $("#loan_term_slider").slider({
      value: parseFloat($("#loan_term").val())
    });
  });

  $("#dependents").change(function () {

    setAnnualExpenses();
    redraw();
  });

  $("#net_salary_type1").change(function () {
    redraw();
  });

  $("#net_salary1").change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#net_salary_type2").change(function () {
    redraw();
  });

  $("#net_salary2").change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#other_net_income_type").change(function () {
    redraw();
  });

  $("#other_net_income").change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#other_payments_type").change(function () {
    redraw();
  });

  $('#car_loan, #credit_card, #other_payments').change(function () {
    redraw();
    $(this).val(LoanCalc.addCommas($(this).val()));
  });

  $("#joint_income").click(function () {
    var joint_income = $('#joint_income:checked').val();

    if (joint_income) {
      $("#net_salary2").removeAttr('readonly').removeClass("readonly");
      $("#net_salary_type2").removeAttr('disabled');

    } else {
      $("#net_salary2").attr('readonly', true).addClass("readonly").val(0);
      $("#net_salary_type2").attr('disabled', 'disabled');
    }

    setAnnualExpenses();

    redraw();
  });

  $("#annual_expenses_default").click(function () {
    var annual_expenses_default = $('#annual_expenses_default:checked').val();
    if (!annual_expenses_default) {
      $("#annual_expenses").removeAttr('readonly').removeClass("readonly");
    } else {
      $("#annual_expenses").attr('readonly', true).addClass("readonly");
    }

    setAnnualExpenses();

    redraw();
  });

  function setAnnualExpenses() {
    var joint_income = $("#joint_income:checked").val();
    var dependents = parseInt($("#dependents").val());
    var annual_expenses_default = $('#annual_expenses_default:checked').val();

    if (!annual_expenses_default)
      return;

    var annual_expenses = "";

    if (!joint_income) {
      switch (dependents) {
        case 0:
          annual_expenses = "15,624";
          break;
        case 1:
          annual_expenses = "21,459";
          break;
        case 2:
          annual_expenses = "26,980";
          break;
        case 3:
          annual_expenses = "32,500";
          break;
        case 4:
          annual_expenses = "38,021";
          break;
      }
    } else {
      switch (dependents) {
        case 0:
          annual_expenses = "22,715";
          break;
        case 1:
          annual_expenses = "28,235";
          break;
        case 2:
          annual_expenses = "33,756";
          break;
        case 3:
          annual_expenses = "39,277";
          break;
        case 4:
          annual_expenses = "44,736";
          break;
      }
    }

    $('#annual_expenses').val(annual_expenses);
  }

  /**
   * @param {string} type
   * @param {number} val
   * @returns {number}
   */
  function getMonthlyValue(type, val) {
    if (type == "annually") {
      val /= MONTHS_PER_YEAR;
    } else if (type == "weekly") {
      val = val * WEEKS_PER_YEAR / MONTHS_PER_YEAR;
    } else if (type == "fortnightly") {
      val = val * FORTNIGHTS_PER_YEAR / MONTHS_PER_YEAR;
    }
    return val;
  }

  function redraw() {
    var inputs = get_inputs();

    var interest = (inputs.interest_rate + inputs.interest_rate_buffer) / 100.0;
    var net_salary1 = getMonthlyValue(inputs.net_salary_type1, inputs.net_salary1);
    var net_salary2 = getMonthlyValue(inputs.net_salary_type2, inputs.net_salary2);
    var other_net_income = getMonthlyValue(inputs.other_net_income_type, inputs.other_net_income);
    var other_payments = getMonthlyValue(inputs.other_payments_type, inputs.other_payments);

    var expenses = (inputs.annual_expenses / MONTHS_PER_YEAR) + inputs.car_loan + inputs.credit_card + other_payments;
    var salary = net_salary1 + net_salary2 + other_net_income;
    var available = (inputs.max_income_available / 100.0) * salary - expenses;
    var n = inputs.loan_term * MONTHS_PER_YEAR;

    interest /= MONTHS_PER_YEAR;

    // http://www.infobarrel.com/Financial_Math%3A__How_Much_Can_I_Afford_to_Borrow%3F
    var loan_amount = (available / interest) * (( Math.pow((1.0 + interest), n) - 1.0 ) / ( Math.pow((1.0 + interest), n) ));

    loan_amount = loan_amount - loan_amount % 1000;		// round down to nearest thousand

    var normal_interest = ((inputs.interest_rate ) / 100.0) / 12;
    var pmt = LoanCalc.PMT(normal_interest, n, loan_amount, 0);

    $("#res1").html("$" + LoanCalc.addCommas(loan_amount.toFixed(0)));
    $("#res2").html("$" + LoanCalc.addCommas(pmt.toFixed(2)));

    var arr = [];

    var principal = loan_amount;
    var total = loan_amount + LoanCalc.totalInterest(normal_interest, n, pmt, loan_amount);
    var interest_payment;
    var principal_payment;

    for (var i = 0; i < n; i++) {
      interest_payment = principal * normal_interest;
      principal_payment = pmt - interest_payment;

      arr[i] = {};
      arr[i].original = principal;
      principal -= principal_payment;
      arr[i].total = total;
      total -= pmt;
    }

    (function drawChart(arr, scale) {
      var data = new google.visualization.DataTable();

      data.addColumn('number', 'years');
      data.addColumn('number', 'total');
      data.addColumn('number', 'principal');


      for (var i = 0; i < arr.length; i++) {
        if (arr[i].original < 0)
          arr[i].original = 0;

        if (arr[i].total < 0)
          arr[i].total = 0;

        data.addRow([i/scale, arr[i].total/1000, arr[i].original/1000 ]);
      }

      var options = {
        //title: 'Amount Owing',
        //backgroundColor: "#eee",
        colors: ['#67C5B2','#1B97B9'],
        //colors:['#A2C180','#3D7930','#FFC6A5','#FFFF42','#DEF3BD','#00A5C6','#DEBDDE','#000000'],
        hAxis: {title: 'Years'},
        //vAxis: {title: 'Amount Owing', format:'$#K', minValue: 0,, titleTextStyle: { fontSize: 12 }},
        vAxis: {title: 'Amount Owing', format:'$#K'},
        legend: {position: 'bottom'},
        //animation: { duration: 1000, easing: 'out', }
      };

      var chartElement = document.getElementById('chart_div');

      if (printMode) {
        if ($(chartElement).attr('style')) {
          $(chartElement).removeAttr('style');
        }
        printMode = false;
      }

      var chart = new google.visualization.AreaChart(chartElement);
      chart.draw(data, options);
    }(arr, MONTHS_PER_YEAR));

    result = [
      {key: "joint income", value: inputs.joint_income ? "true" : "false"},
      {key: "dependents", value: inputs.dependents },
      {key: "SEPARATOR"},
      {key: "Net salary interval", value: inputs.net_salary_type1},
      {key: "Net salary", value: "$" + inputs.net_salary1},
      {key: "SEPARATOR"},
      {key: "Net salary 2 interval", value: inputs.net_salary_type2},
      {key: "Net salary 2", value: "$" + inputs.net_salary2},
      {key: "SEPARATOR"},
      {key: "annual expenses", value: "$" + inputs.annual_expenses},
      {key: "car loan", value: "$" + inputs.car_loan},
      {key: "credit card", value: "$" + inputs.credit_card},
      {key: "other payments type", value: inputs.other_payments_type},
      {key: "other payments", value: "$" + inputs.other_payments},
      {key: "max percent income available", value: "$" + inputs.max_income_available},
      {key: "SEPARATOR"},
      {key: "interest rate", value: inputs.interest_rate + "%"},
      {key: "loan term", value: inputs.loan_term + " years"},
      {key: "total interest", value: inputs.interest_rate_buffer + "%"},
      {key: "SEPARATOR"},
      {key: "you can borrow", value: "$" + LoanCalc.addCommas(loan_amount.toFixed(0)) },
      {key: "repayments", value: "$" + LoanCalc.addCommas(pmt.toFixed(2)) }
    ];
  }

  var printModeReset;
  $(".calc .print").click(function () {
    $('#chart_div').width(400);
    $(window).trigger('resize.chart');
    setTimeout(function () {
      window.print();
      printMode = true;
      clearTimeout(printModeReset);
      printModeReset = setTimeout(function () {
        $(window).trigger('resize.chart');
      }, 1);
    }, 100);
  });

  $(".calc .save").click(function (e) {
    e.preventDefault();
    var data = encodeURIComponent(JSON.stringify(result));
    var win=window.open('save.php?data=' + data + '&title=Loan Repayment', '_blank');
    win.focus();
  });

  $(document).on('click', '.calc .email', function (e) {
    e.preventDefault();
    var $form = $('#email-form');
    var isVisible = $form.is(':visible');

    $form[isVisible ? 'hide' : 'show']();
    $(".calc #message").val() //.val(isVisible ? '' : LoanCalc.resultToString(result));
  });

  var resizeTimer;
  $(window).on('resize.chart', function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(redraw, 100);
  });

  $(".calc #send_email").click(function (e) {
    e.preventDefault();
    LoanCalc.sendMail();
  });

  redraw();
})(jQuery);



