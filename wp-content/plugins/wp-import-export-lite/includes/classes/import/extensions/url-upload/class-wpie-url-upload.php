<?php

namespace wpie\import\upload\url;

use WP_Error;

if ( ! defined( 'ABSPATH' ) ) {
        die( __( "Can't load this file directly", 'wp-import-export-lite' ) );
}
if ( file_exists( WPIE_IMPORT_CLASSES_DIR . '/class-wpie-upload.php' ) ) {
        require_once(WPIE_IMPORT_CLASSES_DIR . '/class-wpie-upload.php');
}

class WPIE_URL_Upload extends \wpie\import\upload\WPIE_Upload {

        public function __construct() {
                
        }

        public function wpie_download_file_from_url( $wpie_import_id = 0, $file_url = "" ) {

                if ( empty( $file_url ) ) {

                        return new \WP_Error( 'wpie_import_error', __( 'File URL is empty', 'wp-import-export-lite' ) );
                } elseif ( ! is_dir( WPIE_UPLOAD_IMPORT_DIR ) || ! wp_is_writable( WPIE_UPLOAD_IMPORT_DIR ) ) {

                        return new \WP_Error( 'wpie_import_error', __( 'Uploads folder is not writable', 'wp-import-export-lite' ) );
                }

                $file_url = $this->process_url( $file_url );

                $file_data = $this->download_file( $file_url );

                if ( is_wp_error( $file_data ) ) {

                        return $file_data;
                }

                $fileName = $file_data[ 'name' ] ? $file_data[ 'name' ] : "";

                $tempName = $file_data[ 'tmp_name' ] ? $file_data[ 'tmp_name' ] : "";

                if ( ! preg_match( '%\W(xml|zip|csv|xls|xlsx|xml|ods|txt|json)$%i', trim( $fileName ) ) ) {
                        unlink( $tempName );
                        return new \WP_Error( 'invalid_image', __( 'Invalid File Extension', 'wp-import-export-lite' ) );
                }

                $newfiledir = parent::wpie_create_safe_dir_name( $fileName );

                wp_mkdir_p( WPIE_UPLOAD_IMPORT_DIR . "/" . $newfiledir );

                wp_mkdir_p( WPIE_UPLOAD_IMPORT_DIR . "/" . $newfiledir . "/original" );

                wp_mkdir_p( WPIE_UPLOAD_IMPORT_DIR . "/" . $newfiledir . "/parse" );

                wp_mkdir_p( WPIE_UPLOAD_IMPORT_DIR . "/" . $newfiledir . "/parse/chunks" );

                $filePath = WPIE_UPLOAD_IMPORT_DIR . "/" . $newfiledir . "/original/" . $fileName;

                chmod( WPIE_UPLOAD_IMPORT_DIR . "/" . $newfiledir . "/original/", 0755 );

                copy( $tempName, WPIE_UPLOAD_IMPORT_DIR . "/" . $newfiledir . "/original/" . $fileName );

                unlink( $tempName );

                unset( $file_url, $filePath );

                return parent::wpie_manage_import_file( $fileName, $newfiledir, $wpie_import_id );
        }

        private function process_url( $link = "", $format = 'csv' ) {

                if ( empty( $link ) ) {
                        return $link;
                }

                $link = str_replace( " ", "%20", $link );

                preg_match( '/(?<=.com\/).*?(?=\/d)/', $link, $match );

                if ( isset( $match[ 0 ] ) && ! empty( $match[ 0 ] ) ) {
                        $type = $match[ 0 ];
                } else {
                        $type = null;
                }

                $parse = parse_url( $link );
                $domain = isset( $parse[ 'host' ] ) ? $parse[ 'host' ] : '';
                unset( $match, $parse );

                if ( preg_match( '/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $match ) ) {
                        $domain = isset( $match[ 'domain' ] ) ? $match[ 'domain' ] : "";
                }
                unset( $match );

                if ( ! empty( $domain ) ) {
                        switch ( $domain ) {
                                case 'dropbox.com':
                                        if ( substr( $link, -4 ) == 'dl=0' ) {
                                                $link = str_replace( 'dl=0', 'dl=1', $link );
                                        }
                                        break;
                                case 'google.com':
                                        if ( ! empty( $type ) ) {
                                                switch ( $type ) {
                                                        case 'file':
                                                                $pattern = '/(?<=\/file\/d\/).*?(?=\/edit)/';
                                                                preg_match( $pattern, $link, $match );
                                                                $file_id = isset( $match[ 0 ] ) ? $match[ 0 ] : null;
                                                                if ( ! empty( $file_id ) ) {
                                                                        $link = 'https://drive.google.com/uc?export=download&id=' . $file_id;
                                                                }
                                                                break;
                                                        case 'spreadsheets':
                                                                $pattern = '/(?<=\/spreadsheets\/d\/).*?(?=\/edit)/';
                                                                preg_match( $pattern, $link, $match );
                                                                $file_id = isset( $match[ 0 ] ) ? $match[ 0 ] : null;
                                                                if ( ! empty( $file_id ) ) {
                                                                        $link = 'https://docs.google.com/spreadsheets/d/' . $file_id . '/export?format=' . $format;
                                                                }
                                                                break;
                                                }
                                        }
                                        break;
                        }
                }
                return $link;
        }

        private function download_file( $file_url = "" ) {

                if ( empty( $file_url ) ) {
                        return new \WP_Error( 'http_404', __( 'Empty Image URL', 'wp-import-export-lite' ) );
                }

                $fileName = time() . rand() . ".tmp";

                $filePath = WPIE_UPLOAD_TEMP_DIR . "/" . $fileName;

                $file = $this->download_by_api( $file_url, $filePath );

                if ( is_wp_error( $file ) ) {

                        $curl_file = $this->download_by_curl( $file_url );

                        if ( is_wp_error( $curl_file ) ) {

                                if ( file_exists( $filePath ) ) {
                                        unlink( $filePath );
                                }

                                return $file;
                        } else {
                                $file = $curl_file;
                        }
                }

                return [ "name" => $file, "tmp_name" => $filePath ];
        }

        private function download_by_curl( $url = "", $file = "" ) {

                if ( ! function_exists( 'curl_version' ) ) {
                        return new \WP_Error( 'download_error', __( "Can't access CURL", 'wp-import-export-lite' ) );
                }

                $ch = curl_init( $url );

                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

                curl_setopt( $ch, CURLOPT_HEADER, true );

                curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );

                curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );

                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

                $url_data = parse_url( $url );

                if ( ! ( empty( $url_data[ 'user' ] ) || empty( $url_data[ 'pass' ] )) ) {

                        curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY );

                        curl_setopt( $ch, CURLOPT_USERPWD, $url_data[ 'user' ] . ":" . $url_data[ 'pass' ] );

                        $url = $url_data[ 'scheme' ] . '://' . $url_data[ 'host' ];

                        if ( ! empty( $url_data[ 'port' ] ) ) {
                                $url .= ':' . $url_data[ 'port' ];
                        }

                        $url .= $url_data[ 'path' ];
                        if ( ! empty( $url_data[ 'query' ] ) ) {
                                $url .= '?' . $url_data[ 'query' ];
                        }
                        curl_setopt( $ch, CURLOPT_URL, $url );
                }

                $rawdata = curl_exec( $ch );

                $http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

                $content_type = curl_getinfo( $ch, CURLINFO_CONTENT_TYPE );

                $header_size = curl_getinfo( $ch, CURLINFO_HEADER_SIZE );

                curl_close( $ch );

                $headers_data = ($header_size > 0 && trim( $rawdata ) !== "") ? substr( $rawdata, 0, $header_size ) : "";

                $headers = $this->header_to_array( $headers_data );

                $content_disposition = isset( $headers[ 'content-disposition' ] ) ? $headers[ 'content-disposition' ] : "";

                if ( $http_code !== 200 ) {
                        return new \WP_Error( 'download_error', __( "Invalid Status Code", 'wp-import-export-lite' ) );
                }
                if ( empty( $rawdata ) )
                        return new \WP_Error( 'download_error', __( "File is empty", 'wp-import-export-lite' ) );;

                if ( ! file_put_contents( $file, $rawdata ) ) {
                        $fp = fopen( $file, 'w' );
                        fwrite( $fp, $rawdata );
                        fclose( $fp );
                }

                return $this->get_filename_from_headers( $url, $file, $content_disposition, $content_type );
        }

        private function header_to_array( $headers = "" ) {

                $data = [];

                if ( empty( $headers ) ) {
                        return $data;
                }

                $headers = str_replace( "\r\n", "\n", $headers );

                $headers = preg_replace( '/\n[ \t]/', ' ', $headers );

                $headers = explode( "\n", $headers );

                foreach ( $headers as $header ) {

                        if ( empty( trim( $header ) ) ) {
                                continue;
                        }
                        list($key, $value) = explode( ':', $header, 2 );

                        if ( empty( trim( $key ) ) || empty( trim( $key ) ) ) {
                                continue;
                        }

                        $value = trim( $value );

                        $value = preg_replace( '#(\s+)#i', ' ', $value );

                        $data[ $key ] = $value;
                }

                return $data;
        }

        private function download_by_api( $url = "", $file = "" ) {

                if ( trim( $url ) === "" ) {
                        return "";
                }

                $response = wp_safe_remote_get( $url, [ 'timeout' => 3000, 'stream' => true, 'filename' => $file ] );

                if ( is_wp_error( $response ) ) {

                        return $response;
                }

                if ( 200 != wp_remote_retrieve_response_code( $response ) ) {

                        return new \WP_Error( 'http_404', trim( wp_remote_retrieve_response_message( $response ) ) );
                }

                $content_md5 = wp_remote_retrieve_header( $response, 'content-md5' );

                if ( $content_md5 ) {

                        $md5_check = verify_file_md5( $file, $content_md5 );

                        if ( is_wp_error( $md5_check ) ) {

                                return $md5_check;
                        }

                        unset( $md5_check );
                }
                $content_disposition = wp_remote_retrieve_header( $response, 'content-disposition' );

                $content_type = wp_remote_retrieve_header( $response, 'content-type' );

                return $this->get_filename_from_headers( $url, $file, $content_disposition, $content_type );
        }

        private function get_filename_from_headers( $url, $file, $content_disposition, $content_type ) {


                $filename = $this->get_filename_from_content_disposition( $content_disposition );

                $ext = $this->get_ext_from_content_type( $content_type );

                $url_data = parse_url( $url );

                $url_path = isset( $url_data[ 'path' ] ) ? $url_data[ 'path' ] : "";

                $path_info = trim( $url_path ) !== "" ? pathinfo( $url_path ) : pathinfo( $url );

                $new_ext = isset( $path_info[ 'extension' ] ) ? $path_info[ 'extension' ] : "";

                $new_filename = isset( $path_info[ 'basename' ] ) ? $path_info[ 'basename' ] : "";

                if ( trim( $ext ) !== "" && (trim( $new_ext ) === "" || strtolower( trim( $new_ext ) ) !== strtolower( trim( $ext ) ) ) ) {
                        $new_ext = strtolower( trim( $ext ) );
                }
                if ( trim( $filename ) !== "" && (trim( $new_filename ) === "" || strtolower( trim( $new_filename ) ) !== strtolower( trim( $filename ) ) ) ) {
                        $new_filename = $filename;
                }

                if ( trim( $new_ext ) === "" && trim( $new_filename ) !== "" ) {
                        $new_ext = pathinfo( $new_filename, PATHINFO_EXTENSION );
                }

                if ( trim( $new_ext ) !== "" ) {

                        $temp_ext = pathinfo( $new_filename, PATHINFO_EXTENSION );

                        if ( strtolower( trim( $new_ext ) ) !== strtolower( trim( $temp_ext ) ) ) {
                                $new_filename = pathinfo( $new_filename, PATHINFO_FILENAME ) . "." . $new_ext;
                        }
                }

                $new_filename = preg_replace( "/[^a-z0-9\_\-\.]/i", '', preg_replace( '#[ -]+#', '-', $new_filename ) );

                if ( trim( $new_filename ) === "" ) {
                        $new_filename = pathinfo( $file, PATHINFO_FILENAME );
                }

                return $new_filename;
        }

        private function get_ext_from_content_type( $content_type = "" ) {

                if ( trim( $content_type ) === "" ) {
                        return "";
                }
                $ext = "";

                if ( strpos( $content_type, "text/xml" ) !== false || strpos( $content_type, "application/xml" ) !== false ) {
                        $ext = "xml";
                } elseif ( strpos( $content_type, "text/plain" ) !== false ) {
                        $ext = "txt";
                } elseif ( strpos( $content_type, "text/csv" ) !== false ) {
                        $ext = "csv";
                } elseif ( strpos( $content_type, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ) !== false ) {
                        $ext = "xlsx";
                } elseif ( strpos( $content_type, 'application/vnd.ms-excel' ) !== false ) {
                        $ext = "xls";
                } elseif ( strpos( $content_type, 'application/json' ) !== false || strpos( $content_type, 'json' ) !== false ) {
                        $ext = "json";
                } elseif ( strpos( $content_type, 'application/zip' ) !== false ) {
                        $ext = "zip";
                } elseif ( strpos( $content_type, 'application/vnd.oasis.opendocument.spreadsheet' ) !== false ) {
                        $ext = "ods";
                }

                return $ext;
        }

        private function get_filename_from_content_disposition( $content_disposition = "" ) {

                if ( empty( $content_disposition ) ) {
                        return "";
                }

                $regex = '/.*?filename=(?<fn>[^\s]+|\x22[^\x22]+\x22)\x3B?.*$/m';

                $new_file_data = null;

                $original_name = "";

                if ( preg_match( $regex, $content_disposition, $new_file_data ) ) {

                        if ( isset( $new_file_data[ 'fn' ] ) && ! empty( $new_file_data[ 'fn' ] ) ) {
                                $wp_filetype = wp_check_filetype( $new_file_data[ 'fn' ] );
                                if ( isset( $wp_filetype[ 'ext' ] ) && ( ! empty( $wp_filetype[ 'ext' ] )) && isset( $wp_filetype[ 'type' ] ) && ( ! empty( $wp_filetype[ 'type' ] )) ) {
                                        $original_name = $new_file_data[ 'fn' ];
                                }
                        }
                }

                if ( empty( $original_name ) ) {

                        $regex = '/.*filename=([\'\"]?)([^\"]+)\1/';

                        if ( preg_match( $regex, $content_disposition, $new_file_data ) ) {

                                if ( isset( $new_file_data[ '2' ] ) && ! empty( $new_file_data[ '2' ] ) ) {
                                        $wp_filetype = wp_check_filetype( $new_file_data[ '2' ] );
                                        if ( isset( $wp_filetype[ 'ext' ] ) && ( ! empty( $wp_filetype[ 'ext' ] )) && isset( $wp_filetype[ 'type' ] ) && ( ! empty( $wp_filetype[ 'type' ] )) ) {
                                                $original_name = $new_file_data[ '2' ];
                                        }
                                }
                        }
                }

                return $original_name;
        }

        public function __destruct() {
                foreach ( $this as $key => $value ) {
                        unset( $this->$key );
                }
        }

}
